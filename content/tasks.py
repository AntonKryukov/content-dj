from collections import defaultdict

from celery import task
from django.conf import settings
from django.db.models import F

from kombu import Connection
from kombu.compat import Publisher, Consumer

from content.models import Content


def increment_counter(content_id):
    """Посылаем сообщение, о том, что нужно увеличть счетчик"""
    connection = Connection(settings.CELERY_BROKER_URL)
    publisher = Publisher(connection=connection,
                          exchange="content_views",
                          routing_key="increment_counter",
                          exchange_type="direct")

    publisher.send(content_id)  # тело сообщеия - id

    publisher.close()
    connection.close()


@task
def process_increment_counter_messages():
    """Забираем из очереди сообщения и обновляем счетчики в бд"""
    connection = Connection(settings.CELERY_BROKER_URL)
    consumer = Consumer(connection=connection,
                        queue="content_views",
                        exchange="content_views",
                        routing_key="increment_counter",
                        exchange_type="direct")

    counters = defaultdict(int)

    for message in consumer.iterqueue():
        try:
            counters[int(message.body)] += 1
        except ValueError:
            # тут нужно слать email, что ключ передан не int
            pass
        message.ack()

    # такой выборкой избавлюсь от ошибок, если будут несуществующие id-шники,
    # например, если объект удален
    for content in Content.objects.filter(id__in=counters.keys()):
        content.counter = F('counter') + counters[content.id]
        content.save(update_fields=["counter"])

    consumer.close()
    connection.close()
