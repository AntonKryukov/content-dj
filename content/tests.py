from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from content.models import Page, Audio, Text, Video


class TestPage(APITestCase):
    def setUp(self):
        self.pages_count = 4
        for i in range(self.pages_count):
            p, _ = Page.objects.get_or_create(slug='page-{}'.format(i))
            p.title = 'Page {}'.format(i)
            p.order = i
            p.save()

    def test_page_list_api(self):
        url = reverse('page-list')
        response = self.client.get(url, format='json')
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(self.pages_count, response.data.get('count'))
        for i in range(self.pages_count):
            page = response.data.get('results')[i]
            self.assertEqual('page-{}'.format(i), page.get('slug'))
            self.assertEqual(i, page.get('order'))

    def test_page_detail_api(self):
        url = reverse('page-detail', args=['page-0'])
        response = self.client.get(url, format='json')
        self.assertEqual(status.HTTP_200_OK, response.status_code)


class TestContent(APITestCase):
    def setUp(self):
        self.page, _ = Page.objects.get_or_create(slug='page-0',
                                                  defaults={'title': 'Page-0',
                                                            'order': 0})

        Audio.objects.create(page=self.page, title='Audio', order=0,
                             audio_file='/test_audio_file.mp3', bitrate=1440)
        Text.objects.create(page=self.page, title='Text', order=1,
                            text_field='test text')
        Video.objects.create(page=self.page, title='Video', order=2,
                             video_file='/test_video_file.mp4',
                             subtitles_file='/test_subtitles_file.txt')

    def test_page_with_content(self):
        url = reverse('page-detail', args=[self.page.slug])

        response = self.client.get(url, format='json')
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertIsNotNone(response.data.get('content'))
        self.assertEqual(3, len(response.data.get('content')))
        for content in response.data.get('content'):
            resource_type = content.get('resourcetype')

            if resource_type == 'Video':
                self.assertTrue(content.get('video_file', '').endswith(
                    '/test_video_file.mp4'))
                self.assertTrue(content.get('subtitles_file', '').endswith(
                    '/test_subtitles_file.txt'))

            if resource_type == 'Audio':
                self.assertEqual(1440, content.get('bitrate'))

                self.assertTrue(content.get('audio_file', '').endswith(
                    '/test_audio_file.mp3'))

            if resource_type == 'Text':
                self.assertEqual('test text', content.get('text_field'))
