from rest_framework import serializers
from rest_polymorphic.serializers import PolymorphicSerializer

from content.models import Page, Content, Video, Audio, Text


class ContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Content
        fields = ('title', 'order', 'counter')


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = ('title', 'order', 'counter', 'video_file', 'subtitles_file')


class AudioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Audio
        fields = ('title', 'order', 'counter', 'audio_file', 'bitrate')


class TextSerializer(serializers.ModelSerializer):
    class Meta:
        model = Text
        fields = ('title', 'order', 'counter', 'text_field')


class ContentPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        Content: ContentSerializer,
        Video: VideoSerializer,
        Audio: AudioSerializer,
        Text: TextSerializer
    }


class PageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Page
        fields = 'url', 'title', 'slug', 'order'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }


class PageContentsSerializer(serializers.ModelSerializer):
    content = ContentPolymorphicSerializer(many=True, allow_null=True)

    class Meta:
        model = Page
        fields = 'title', 'slug', 'order', 'content'
