from django.urls import path

from content.views import PageList, PageDetail, api_root

urlpatterns = [
    path('pages/', PageList.as_view(), name='page-list'),
    path('pages/<slug:slug>/', PageDetail.as_view(), name='page-detail'),
    path('', api_root),
]
