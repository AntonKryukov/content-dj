from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse

from content.models import Page
from content.serializers import PageSerializer, PageContentsSerializer
from content.tasks import increment_counter


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'pages': reverse('page-list', request=request, format=format)
    })


class PageList(generics.ListAPIView):
    queryset = Page.objects.all()
    serializer_class = PageSerializer


class PageDetail(generics.RetrieveAPIView):  # todo переделать на slug
    queryset = Page.objects.all()
    serializer_class = PageContentsSerializer
    lookup_field = 'slug'

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        page = self.get_object()
        for c in page.content.all():
            increment_counter(c.id)
        return response
