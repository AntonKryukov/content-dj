from django import forms
from django.contrib import admin
from django.db.models import Q
from polymorphic.admin import StackedPolymorphicInline, \
    PolymorphicInlineSupportMixin

from content.models import Page, Content, Video, Audio, Text


class ContentInline(StackedPolymorphicInline):
    class VideoInline(StackedPolymorphicInline.Child):
        model = Video
        fields = (('title', 'order', 'counter'),
                  'video_file', 'subtitles_file')
        readonly_fields = 'counter',

    class AudioInline(StackedPolymorphicInline.Child):
        model = Audio
        fields = (('title', 'order', 'counter'),
                  'audio_file', 'bitrate')
        readonly_fields = 'counter',

    class TextInline(StackedPolymorphicInline.Child):
        model = Text
        fields = (('title', 'order', 'counter'),
                  'text_field')
        readonly_fields = 'counter',

    model = Content
    child_inlines = (
        VideoInline,
        AudioInline,
        TextInline,
    )


class PageAdminForm(forms.ModelForm):
    def is_multipart(self):
        return True


@admin.register(Page)
class PageAdmin(PolymorphicInlineSupportMixin, admin.ModelAdmin):
    form = PageAdminForm

    list_display = 'title', 'slug', 'has_content', 'order'
    list_editable = 'order',
    inlines = (ContentInline,)
    search_fields = 'title',
    prepopulated_fields = {'slug': ('title',), }

    def has_content(self, object):
        return object.content.count() > 0

    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super().get_search_results(request, queryset,
                                                            search_term)
        queryset |= self.model.objects.filter(
            Q(content__title__startswith=search_term))
        use_distinct = True
        return queryset, use_distinct
