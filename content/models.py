from django.db import models
from django.utils.translation import gettext as _
from polymorphic.models import PolymorphicModel


class Page(models.Model):
    title = models.CharField(_("Title"), max_length=250)
    slug = models.SlugField(_("Slug"), max_length=250, unique=True)
    order = models.IntegerField(_("Order"), default=1)

    class Meta:
        verbose_name = _("Page")
        verbose_name_plural = _("Page list")
        ordering = ['order']

    def __str__(self):
        return self.title


class Content(PolymorphicModel):
    page = models.ForeignKey(Page, on_delete=models.CASCADE,
                             related_name='content')
    title = models.CharField(_("Title"), max_length=250)
    order = models.IntegerField(_("Order"), default=1)
    counter = models.IntegerField(_("View counter"), default=0)

    class Meta:
        ordering = ['order']
        verbose_name = _("Content")
        verbose_name_plural = _("Content list")

    def __str__(self):
        return self.title


class Video(Content):
    video_file = models.FileField(_("Video file"), upload_to='uploads/video')
    subtitles_file = models.FileField(_("Subtitles file"),
                                      upload_to='uploads/subtitles')

    class Meta:
        verbose_name = _("Video")
        verbose_name_plural = _("Video list")


class Audio(Content):
    audio_file = models.FileField(_("Audio file"), upload_to='uploads/audio')
    bitrate = models.IntegerField(_("Bitrate"), help_text=_("bit per second"))

    class Meta:
        verbose_name = _("Audio")
        verbose_name_plural = _("Audio list")


class Text(Content):
    text_field = models.TextField(_("Text"))

    class Meta:
        verbose_name = _("Text")
        verbose_name_plural = _("Text list")
